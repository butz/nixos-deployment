{
  pkgs,
  inputs,
  allModules,
}: {
  meta = {
    nixpkgs = pkgs;
    specialArgs.flakes = inputs;
  };

  defaults = {
    config,
    lib,
    name,
    ...
  }: {
    deployment = {
      targetHost = lib.mkDefault "${name}.club.muc.ccc.de";
      targetUser = lib.mkDefault null;
      tags = [config.nixpkgs.system];
    };
    imports =
      allModules
      ++ [
        "${inputs.self}/hosts/${name}.nix"
      ];
  };

  briafzentrum = {
    deployment.targetHost = "briafzentrum.muc.ccc.de";
    muccc.qemu-guest.enable = true;
  };

  nixbus = {
    muccc.qemu-guest.enable = true;
  };

  loungepi = {
    muccc.rpi.enable = true;
  };

  hauptraumpi = {
    muccc.rpi.enable = true;
  };

  luftschleuse = {
    muccc.rpi.enable = true;
    nixpkgs.system = "aarch64-linux";
  };

  prometheus = {
    muccc.qemu-guest.enable = true;
  };

  brezn = {
    deployment.targetHost = "brezn.muc.ccc.de";
    deployment.tags = ["dns"];
    muccc.qemu-guest.enable = true;
  };

  zonk = {
    deployment.targetHost = "zonk.muc.ccc.de";
    deployment.tags = ["dns"];
    muccc.qemu-guest.enable = true;
  };

  zock = {
    muccc.qemu-guest.enable = true;
  };

  nginx = {
    muccc.qemu-guest.enable = true;
  };

  auth = {
    muccc.qemu-guest.enable = true;
  };

  gitlab-runner = {
    muccc.qemu-guest.enable = true;
  };

  nextcloud = {
    muccc.qemu-guest.enable = true;
  };

  netbox = {
    muccc.qemu-guest.enable = true;
  };

  pad = {
    muccc.qemu-guest.enable = true;
  };

  gitea = {
    muccc.qemu-guest.enable = true;
  };

  oaarchkatzl = {
    deployment.targetHost = "oaarchkatzl.muc.ccc.de";
    deployment.buildOnTarget = true;
    muccc.hcloud.aarch64.enable = true;
    services.cloud-init.enable = false;
  };
}

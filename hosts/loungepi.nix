{
  system.stateVersion = "23.05";

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/LOUNGEPI_NIXOS";
    };
  };

  networking = {
    wireless = {
      enable = false; # for bluetooth audio
      networks = {
        "muccc.legacy-5GHz".psk = "haileris";
        "muccc.legacy-2.4GHz".psk = "haileris";
      };
      interfaces = ["wlan"];
    };
    firewall = {
      trustedInterfaces = ["wlan" "upl0nk"];
    };
  };

  systemd.network.links."30-wlan" = {
    matchConfig.PermanentMACAddress = "dc:a6:32:b3:d0:74";
    linkConfig.Name = "wlan";
  };
  systemd.network.networks."30-wlan" = {
    matchConfig.Name = "wlan";
    linkConfig.RequiredForOnline = false;
    networkConfig = {
      DHCP = "yes";
      LLMNR = false;
    };
    dhcpV4Config.RouteMetric = 42;
    dhcpV6Config.RouteMetric = 42;
    ipv6AcceptRAConfig.RouteMetric = 42;
  };

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "dc:a6:32:b3:d0:73";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      DHCP = "yes";
      LLMNR = false;
    };
    dhcpV4Config = {
      ClientIdentifier = "mac";
      RouteMetric = 23;
    };
    dhcpV6Config.RouteMetric = 23;
    ipv6AcceptRAConfig.RouteMetric = 23;
  };

  muccc.multimedia = {
    enable = true;
    username = "lounge";
    autostartTmuxp = true;
  };
}

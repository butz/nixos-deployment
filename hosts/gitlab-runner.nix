{
  config,
  pkgs,
  lib,
  ...
}: {
  system.stateVersion = "23.05";

  sops = {
    secrets.gitlab_registration_data = {};
    secrets.gitea_registration_token = {};
  };

  networking = {
    hostName = "gitlab-runner";
    domain = "club.muc.ccc.de";
    firewall.allowedTCPPorts = [8093 9090];
  };

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "DE:D3:39:B8:FC:74";
    linkConfig.Name = "upl0nk";
  };

  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      Address = [
        "83.133.178.90/26"
      ];
      Gateway = [
        "83.133.178.65"
      ];
    };
  };

  virtualisation.docker = {
    enable = true;
    listenOptions = [
      "/run/docker.sock"
      # gitlab-runner connects via tcp to docker
      # hardcoded on the machine in /var/lib/gitlab-runner/.gitlab-runner/config.toml
      "127.0.0.1:2375"
    ];
    autoPrune = {
      enable = true;
      dates = "daily";
    };
  };

  # gitea-actions-runner does not pull docker images
  systemd.timers.pull-docker-image = {
    wantedBy = ["timers.target"];
    timerConfig = {
      OnCalendar = "hourly";
    };
  };
  systemd.services.pull-docker-image = {
    path = with pkgs; [docker gnused];
    script = ''
      for image in $(docker image ls --format '{{.Repository}}:{{.Tag}}' | sed -e 's,:<none>,,'); do
        docker pull $image;
      done
    '';
    serviceConfig.Type = "oneshot";
  };

  systemd.services.gitea-runner-ci.serviceConfig.ExecStart = let
    configFile = (pkgs.formats.yaml {}).generate "gitea-runner.yaml" {runner.capacity = 4;};
  in
    lib.mkForce "${pkgs.gitea-actions-runner}/bin/act_runner daemon -c ${configFile}";

  services.gitea-actions-runner.instances = {
    "ci" = {
      enable = true;
      name = "ci";
      url = "https://gitea.muc.ccc.de";
      tokenFile = config.sops.secrets.gitea_registration_token.path;
      labels = [
        "nix:docker://gitea.muc.ccc.de/muccc/nix:latest"
        "ubuntu-latest:docker://ghcr.io/catthehacker/ubuntu:act-latest"
        "ubuntu-22.04:docker://ghcr.io/catthehacker/ubuntu:act-22.04"
        "ubuntu-20.04:docker://ghcr.io/catthehacker/ubuntu:act-20.04"
        "ubuntu-js-latest:docker://ghcr.io/catthehacker/ubuntu:js-latest"
        "ubuntu-js-22.04:docker://ghcr.io/catthehacker/ubuntu:js-22.04"
        "ubuntu-js-20.04:docker://ghcr.io/catthehacker/ubuntu:js-20.04"
        "ubuntu-rust-latest:docker://ghcr.io/catthehacker/ubuntu:rust-latest"
        "ubuntu-rust-22.04:docker://ghcr.io/catthehacker/ubuntu:rust-22.04"
        "ubuntu-rust-20.04:docker://ghcr.io/catthehacker/ubuntu:rust-20.04"
        "renovate:docker://renovate/renovate:36"
      ];
    };
  };

  muccc.nix-cache = {
    enable = true;
  };
}

{
  system.stateVersion = "23.05";

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/nixos";
    };

    "/boot" = {
      device = "/dev/disk/by-label/boot";
      fsType = "vfat";
    };
  };

  networking = {
    hostName = "nginx";
    domain = "club.muc.ccc.de";
  };

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "5e:e0:1e:54:91:1f";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      Address = [
        "83.133.178.74/26"
      ];
      Gateway = [
        "83.133.178.65"
      ];
      DNS = ["83.133.178.65"];
    };
  };

  services.nginx = {
    enable = true;

    virtualHosts."nginx.club.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;

      default = true;
      locations."/".return = "200 ''";
    };

    virtualHosts."pads.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      locations."/".return = "301 https://muc.pads.ccc.de$request_uri";
    };

    virtualHosts."pve.club.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      locations = {
        "/" = {
          proxyPass = "https://83.133.178.40:8006";
          proxyWebsockets = true;
        };
      };
    };

    virtualHosts."cache.muc.ccc.de" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "http://gitlab-runner.club.muc.ccc.de:8080";
        extraConfig = ''
          client_max_body_size 2G;
          proxy_request_buffering off;
          proxy_connect_timeout 120s;
          proxy_read_timeout 180s;
          proxy_send_timeout 180s;
        '';
      };
    };
  };
}

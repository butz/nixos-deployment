{
  description = "MuCCC Infrastructure Deployment";

  nixConfig = {
    extra-substituters = [
      "https://cache.muc.ccc.de/muccc"
      "https://nix-community.cachix.org"
    ];
    extra-trusted-public-keys = [
      "muccc:ATRH5jlmJyn4kpMmnhMVIbEugyr8lsTYvi5KmadQvZk="
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
    ];
  };

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";

    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        nixpkgs-stable.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };

    docker-nixpkgs = {
      url = "github:fpletz/docker-nixpkgs";
      flake = false;
    };

    muccc-api = {
      url = "git+https://gitea.muc.ccc.de/muCCC/api";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };

    luftschleuse2 = {
      url = "github:muccc/luftschleuse2/luftschleuse3";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };

    home-manager = {
      url = "github:nix-community/home-manager/release-23.05";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        nixpkgs-stable.follows = "nixpkgs";
      };
    };

    mumble-exporter = {
      url = "github:mguentner/mumble_exporter";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    authentik = {
      url = "github:mayflower/authentik-nix/version/2023.10.2";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };

    attic = {
      url = "github:zhaofengli/attic";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        nixpkgs-stable.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };

    nix-fast-build = {
      url = "github:Mic92/nix-fast-build";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    pre-commit-hooks,
    docker-nixpkgs,
    nixos-generators,
    attic,
    nix-fast-build,
    ...
  } @ inputs: let
    supportedSystems = ["x86_64-linux" "aarch64-linux"];
    allModules = nixpkgs.lib.filesystem.listFilesRecursive ./modules;
  in
    flake-utils.lib.eachSystem supportedSystems (system: let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [
          attic.overlays.default
        ];
      };
    in {
      legacyPackages = pkgs;

      devShells.default = pkgs.mkShellNoCC {
        packages = with pkgs; [
          colmena
          age
          sops
          nil
          attic-client
          nix-fast-build.packages.${system}.default
        ];
        shellHook =
          ''
            ln -sf ${self.packages.${system}.sopsYaml} .sops.yaml
          ''
          + self.checks.${system}.pre-commit-check.shellHook;
      };

      checks = {
        pre-commit-check = pre-commit-hooks.lib.${system}.run {
          src = ./.;
          hooks = {
            alejandra.enable = true;
            statix.enable = true;
            nil.enable = true;
          };
        };
      };

      apps = {
        sops-updatekeys = flake-utils.lib.mkApp {
          drv = pkgs.writers.writeBashBin "sops-updatekeys" ''
            for secretfn in secrets/*.yaml; do
              ${pkgs.sops}/bin/sops updatekeys $secretfn
            done
          '';
        };

        issue-after-flake-update = flake-utils.lib.mkApp {
          drv = pkgs.callPackage ./pkgs/issue-after-flake-update.nix {};
        };
      };

      formatter = pkgs.alejandra;

      packages = {
        sopsYaml = pkgs.callPackage ./pkgs/sops.yaml.nix {inherit self;};

        # docker image for nix in gitea actions
        # includes nodejs to run actions
        #  * nix build .#giteaRunnerImage
        #  * docker load -i result
        giteaRunnerImage = pkgs.callPackage ./pkgs/gitea-runner-image.nix {inherit inputs;};

        nix-fast-build = nix-fast-build.packages.${system}.default;
      };
    })
    // (let
      pkgs = import nixpkgs {system = "x86_64-linux";};
    in {
      lib = {
        hosts = import ./lib/hosts.nix;
        humanoids = import ./lib/humanoids.nix;
      };

      # build using:
      #  * the base image: nix build .#proxmoxImages.base
      #  * all other hosts: nix build .#proxmoxImages.nixbus
      # import into proxmox: https://nixos.wiki/wiki/Proxmox_Virtual_Environment#Deploying_on_proxmox
      proxmoxImages =
        {
          base = nixos-generators.nixosGenerate {
            inherit pkgs;
            format = "proxmox";
            modules =
              allModules
              ++ [
                ./profiles/proxmox-image.nix
              ];
            specialArgs = {
              name = "muccc-base";
              flakes = inputs;
            };
          };
        }
        // nixpkgs.lib.mapAttrs (
          name: node: let
            image = node.extendModules {
              modules = [
                nixos-generators.nixosModules.proxmox
                ./modules/proxmox-image.nix
              ];
            };
          in
            image.config.system.build.${image.config.formatAttr}
        )
        (nixpkgs.lib.filterAttrs
          (n: v: v.config.nixpkgs.system == "x86_64-linux")
          self.nixosConfigurations);

      sdImages =
        nixpkgs.lib.mapAttrs (
          name: node: let
            image = node.extendModules {
              modules = [
                nixos-generators.nixosModules.sd-aarch64
              ];
            };
          in
            image.config.system.build.${image.config.formatAttr}
        )
        (nixpkgs.lib.filterAttrs
          (n: v: v.config.nixpkgs.system == "aarch64-linux")
          self.nixosConfigurations);

      # switch to configurations manually using the flake:
      #  * local machine: nixos-rebuild switch --flake .#nixbus
      #  * remote: nixos-rebuild switch --flake .#nixbus --target-host 192.168.1.1
      nixosConfigurations =
        (import "${pkgs.colmena.src}/src/nix/hive/eval.nix" {
          rawFlake = self;
          colmenaOptions = import "${pkgs.colmena.src}/src/nix/hive/options.nix";
          colmenaModules = import "${pkgs.colmena.src}/src/nix/hive/modules.nix";
          hermetic = true;
        })
        .nodes;

      # regular colmena hive
      colmena = import ./hive.nix {inherit pkgs inputs allModules;};
    });
}
